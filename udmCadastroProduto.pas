unit udmCadastroProduto;

interface

uses
  SysUtils, Classes, DBXpress, DB, SqlExpr, FMTBcd, DBClient;

type
  TdmCadastroProduto = class(TDataModule)
    connCadastroProdutos: TSQLConnection;
    qrTransacional: TSQLQuery;
    cdCadastroProdutos: TClientDataSet;
    cdCadastroProdutosIdentificador_do_Cliente: TIntegerField;
    cdCadastroProdutosProduto: TStringField;
    cdCadastroProdutosCodigo_de_barra: TStringField;
  private
    procedure limpaSQL(query: TSQLQuery);
    function persisteOsDados(const sql: String): boolean;
    { Private declarations }
  public
    function carregaProdutos: TSQLQuery;
    function excluiProduto(identificador_do_cliente: integer): boolean;
    function insereProduto(const codigo_de_barra, produto: String; identificador_do_cliente: integer): boolean;
    function alteraProduto(const codigo_de_barra, produto: String; identificador_do_cliente, antigo_ID: integer): boolean;

    { Public declarations }
  end;

var
  dmCadastroProduto: TdmCadastroProduto;

implementation

uses StrUtils;


{$R *.dfm}

{ TdmCadastroProduto }


procedure TdmCadastroProduto.limpaSQL(query: TSQLQuery);
begin
  query.Close;
  query.SQL.Clear;
end;

function TdmCadastroProduto.carregaProdutos: TSQLQuery;
begin
  result := TSQLQuery.Create(self);
  result.SQLConnection := connCadastroProdutos;
  limpaSQL(Result);
  Result.SQL.Add('SELECT IDENTIFICADOR_DO_CLIENTE, PRODUTO, CODIGO_DE_BARRA FROM PRODUTO');
  Result.Open;
  Result.First;
end;

function TdmCadastroProduto.excluiProduto(identificador_do_cliente: integer): boolean;
begin
  result := persisteOsDados('DELETE FROM PRODUTO WHERE IDENTIFICADOR_DO_CLIENTE = ' + intToStr(identificador_do_cliente));
end;

function TdmCadastroProduto.insereProduto(const codigo_de_barra, produto: String; identificador_do_cliente: integer): boolean;
var sSql: String;
begin
  sSQL := 'INSERT INTO PRODUTO(IDENTIFICADOR_DO_CLIENTE, PRODUTO';

  if trim(codigo_de_barra) <> '' then
    sSQL := sSQL + ', CODIGO_DE_BARRA';

  sSQL := sSQL + ') VALUES('+ intToStr(identificador_do_cliente) + ', ' + #39 + produto + #39;

  if trim(codigo_de_barra) <> '' then
    sSQL := sSQL + ', ' + #39 + codigo_de_barra + #39;

  sSQL := sSQL + ')';

  result := persisteOsDados(sSQL);
end;

function TdmCadastroProduto.alteraProduto(const codigo_de_barra, produto: String; identificador_do_cliente, antigo_ID: integer): boolean;
var sSql: String;
begin
  sSQL := 'UPDATE PRODUTO SET IDENTIFICADOR_DO_CLIENTE = ' + intToStr(identificador_do_cliente) +
                           ', PRODUTO = ' + #39 + produto + #39 +
          IfThen(Trim(codigo_de_barra) <> '', ', CODIGO_DE_BARRA = ' + #39 + codigo_de_barra + #39, ', CODIGO_DE_BARRA = NULL') +
          ' WHERE IDENTIFICADOR_DO_CLIENTE = ' + intToStr(antigo_ID);
  result := persisteOsDados(sSQL);
end;

function TdmCadastroProduto.persisteOsDados(const sql: String): boolean;
var transact: TTransactionDesc;
begin
  result := true;
  LimpaSQL(qrTransacional);

  if not qrTransacional.SQLConnection.InTransaction then
  begin
    transact.TransactionID := 1;
    transact.IsolationLevel := xilREADCOMMITTED;

    qrTransacional.SQLConnection.StartTransaction(Transact);
    try
      qrTransacional.SQL.Add(sql);
      qrTransacional.ExecSQL;
      qrTransacional.SQLConnection.Commit(Transact);
    except
      result := false;
      qrTransacional.SQLConnection.Rollback(Transact);
    end;
  end;
end;

end.
