unit uClasseProdutos;

interface

uses SysUtils, Classes, Contnrs, DBXpress, DB, SqlExpr, FMTBcd, DBClient, udmCadastroProduto;

type
  TProduto = class
  private
    produto: String;
    identificador_do_cliente: integer;
    codigo_de_barra: String;
  public
    constructor create(const produto: String; identificador_do_cliente: integer);
    function comCodigoDeBarra(const codigo_de_barra: String):TProduto;
    property getProduto: String read produto;
    property getIdentificadorDoCliente: integer read identificador_do_cliente;
    property getCodigoDeBarra: String read codigo_de_barra;
  end;

  TListaProduto = class(TObjectList)
  private
    dmCadastroProdutos : TdmCadastroProduto;
    function GetProduto(Index: integer): TProduto;
    procedure listaTodosOsProdutos;
    function JaExiste_IdCliente(ID_Cliente, indice: integer):boolean;
    function JaExiste_CodigoDeBarra(const Cod_Barra: String; indice: integer): boolean;
  public
    constructor Create;
    destructor Destroy; override;
    property Produto[Index: Integer]: TProduto read GetProduto; default;
    function carregaDataset: TClientDataSet;
    function excluiProduto(index:integer): boolean;
    function ValidaOperacao(const Cod_Barra: String; Id_Cliente, indice: integer): String;
    function insereProduto(produto: TProduto): boolean;
    function alteraProduto(produto: TProduto; ID_Cliente_Antigo: integer): boolean;
  end;

implementation


{ TListaProduto }

constructor TListaProduto.Create;
begin
  dmCadastroProdutos := TdmCadastroProduto.Create(nil);
  dmCadastroProdutos.connCadastroProdutos.Connected := true;
end;

function TListaProduto.GetProduto(Index: integer): TProduto;
begin
  result := TProduto(Items[Index]);
end;

procedure TListaProduto.listaTodosOsProdutos;
var query: TSQLQuery;
begin
  query := dmCadastroProdutos.CarregaProdutos;

  while not query.Eof do
  begin
    self.Add(TProduto.create(query.FieldByName('PRODUTO').AsString,
                             query.FieldByName('IDENTIFICADOR_DO_CLIENTE').AsInteger)
                             .comCodigoDeBarra(query.FieldByName('CODIGO_DE_BARRA').AsString));
    query.Next;
  end;
  query.Close;
  query.Free;
end;

function TListaProduto.carregaDataset: TClientDataSet;
var i: integer;
begin
  self.Clear;
  listaTodosOsProdutos;
  result := dmCadastroProdutos.cdCadastroProdutos;
  result.Close;
  if result.State <> dsBrowse then
  begin
    result.CreateDataSet;
    result.EmptyDataSet;
  end;

  if self <> nil then
    for i := 0 to self.Count -1 do
    begin
      result.Append;
      result.FieldByName('Identificador_Do_Cliente').AsInteger := self[i].getIdentificadorDoCliente;
      result.FieldByName('Produto').AsString := self[i].getProduto;
      result.FieldByName('Codigo_de_barra').AsString := self[i].getCodigoDeBarra;
      result.Post;
    end;
end;

destructor TListaProduto.Destroy;
begin
  dmCadastroProdutos.connCadastroProdutos.Connected := false;
  dmCadastroProdutos.Free;
  inherited;
end;

function TListaProduto.excluiProduto(index: integer): boolean;
begin
  result := dmCadastroProdutos.excluiProduto(dmCadastroProdutos.cdCadastroProdutosIdentificador_do_Cliente.AsInteger);
  if result then
  begin
    self.Delete(index);
    dmCadastroProdutos.cdCadastroProdutos.Delete;
  end;
end;

function TListaProduto.JaExiste_IdCliente(ID_Cliente, indice: integer): boolean;
var i: integer;
    inclusao: boolean;
begin
  result := false;
  inclusao := indice = -1;
  for i:= 0 to self.Count -1 do
    if ((inclusao) and (self[i].getIdentificadorDoCliente = ID_Cliente)) or
       ((not inclusao) and (self[i].getIdentificadorDoCliente = ID_Cliente) and (indice <> i)) then
    begin
      result := true;
      break;
    end;
end;

function TListaProduto.JaExiste_CodigoDeBarra(const Cod_Barra: String; indice: integer): boolean;
var i: integer;
    inclusao: boolean;
begin
  result := false;
  inclusao := indice = -1;
  for i:= 0 to self.Count -1 do
    if ((inclusao) and (self[i].getCodigoDeBarra = Cod_Barra)) or
       ((not inclusao) and (self[i].getCodigoDeBarra = Cod_Barra) and (indice <> i)) then
    begin
      result := true;
      break;
    end;
end;

function TListaProduto.ValidaOperacao(const Cod_Barra: String; Id_Cliente, indice: integer): String;
begin
  result := '';

  if JaExiste_IdCliente(ID_Cliente, indice) then
  begin
    result := 'Identificador j� cadastrado em outro Produto.';
    exit;
  end;

  if (Trim(Cod_Barra) <> '') and (JaExiste_CodigoDeBarra(Cod_Barra, indice)) then
  begin
    result := 'O c�digo de barra n�o pode ser duplicada';
    exit;
  end;
end;

function TListaProduto.insereProduto(produto: TProduto): boolean;
begin
  result := dmCadastroProdutos.insereProduto(produto.getCodigoDeBarra, produto.getProduto, produto.getIdentificadorDoCliente);
end;

function TListaProduto.alteraProduto(produto: TProduto; ID_Cliente_Antigo: integer): boolean;
begin
  result := dmCadastroProdutos.alteraProduto(produto.getCodigoDeBarra, produto.getProduto, produto.getIdentificadorDoCliente, ID_Cliente_Antigo);
end;

{ TProduto }

function TProduto.comCodigoDeBarra(const codigo_de_barra: String): TProduto;
begin
  self.codigo_de_barra := codigo_de_barra;
  result := self;
end;

constructor TProduto.create(const produto: String; identificador_do_cliente: integer);
begin
  self.produto := produto;
  self.identificador_do_cliente := identificador_do_cliente;
end;

end.
