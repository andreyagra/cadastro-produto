object dmCadastroProduto: TdmCadastroProduto
  OldCreateOrder = False
  Left = 515
  Top = 183
  Height = 203
  Width = 291
  object connCadastroProdutos: TSQLConnection
    ConnectionName = 'IBConnection'
    DriverName = 'Interbase'
    GetDriverFunc = 'getSQLDriverINTERBASE'
    LibraryName = 'dbexpint.dll'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Interbase'
      'Database=base.fdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'Interbase TransIsolation=ReadCommited'
      'Trim Char=False')
    VendorLib = 'fbclient.dll'
    Connected = True
    Left = 40
    Top = 16
  end
  object qrTransacional: TSQLQuery
    MaxBlobSize = -1
    Params = <>
    SQLConnection = connCadastroProdutos
    Left = 208
    Top = 24
  end
  object cdCadastroProdutos: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 116
    object cdCadastroProdutosIdentificador_do_Cliente: TIntegerField
      DisplayLabel = 'Identificador'
      DisplayWidth = 70
      FieldName = 'Identificador_do_Cliente'
    end
    object cdCadastroProdutosProduto: TStringField
      DisplayWidth = 301
      FieldName = 'Produto'
      Size = 60
    end
    object cdCadastroProdutosCodigo_de_barra: TStringField
      DisplayLabel = 'C'#243'digo de Barra'
      DisplayWidth = 85
      FieldName = 'Codigo_de_barra'
      Size = 10
    end
  end
end
