object frmCadastroProduto: TfrmCadastroProduto
  Left = 536
  Top = 124
  Width = 549
  Height = 419
  Caption = 'Cadastro de Produtos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnDados: TPanel
    Left = 0
    Top = 0
    Width = 541
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object grProdutos: TDBGrid
      Left = 0
      Top = 0
      Width = 541
      Height = 360
      Align = alClient
      DataSource = dsCadastroProdutos
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Identificador_do_cliente'
          Title.Caption = 'Identificador'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Produto'
          Width = 301
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo_de_barra'
          Width = 85
          Visible = True
        end>
    end
  end
  object pnNavegacao: TPanel
    Left = 0
    Top = 360
    Width = 541
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object btExcluir: TBitBtn
      Left = 64
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Excluir'
      Default = True
      TabOrder = 0
      OnClick = btExcluirClick
      NumGlyphs = 2
    end
    object btIncluir: TBitBtn
      Left = 172
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Incluir'
      TabOrder = 1
      OnClick = btIncluirClick
    end
    object btAlterar: TBitBtn
      Left = 280
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Alterar'
      TabOrder = 2
      OnClick = btAlterarClick
    end
    object btSair: TBitBtn
      Left = 389
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Sair'
      TabOrder = 3
      OnClick = btSairClick
    end
  end
  object dsCadastroProdutos: TDataSource
    Left = 72
    Top = 116
  end
end
