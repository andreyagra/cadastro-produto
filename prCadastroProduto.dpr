program prCadastroProduto;

uses
  Forms,
  uCadastroProduto in 'uCadastroProduto.pas' {frmCadastroProduto},
  udmCadastroProduto in 'udmCadastroProduto.pas' {dmCadastroProduto: TDataModule},
  uClasseProdutos in 'uClasseProdutos.pas',
  uManutencaoProduto in 'uManutencaoProduto.pas' {frmManutencao};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmCadastroProduto, frmCadastroProduto);
  Application.Run;
end.
