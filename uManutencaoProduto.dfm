object frmManutencao: TfrmManutencao
  Left = 501
  Top = 210
  Width = 427
  Height = 236
  Caption = 'Manuten'#231#227'o dos Dados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnControles: TPanel
    Left = 0
    Top = 168
    Width = 419
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btCancelar: TButton
      Left = 328
      Top = 10
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 0
      OnClick = btCancelarClick
    end
    object btOk: TButton
      Left = 224
      Top = 10
      Width = 75
      Height = 25
      Caption = '&Ok'
      TabOrder = 1
      OnClick = btOkClick
    end
  end
  object pnDados: TPanel
    Left = 0
    Top = 0
    Width = 419
    Height = 168
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 65
      Height = 13
      Caption = 'Identificador*:'
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 44
      Height = 13
      Caption = 'Produto*:'
    end
    object Label3: TLabel
      Left = 16
      Top = 106
      Width = 79
      Height = 13
      Caption = 'C'#243'digo de Barra:'
    end
    object edIdentificadorCliente: TMaskEdit
      Left = 16
      Top = 24
      Width = 152
      Height = 21
      Hint = 'O Identificador '#233' um campo obrigat'#243'rio'
      EditMask = '!9999999999;1; '
      MaxLength = 10
      TabOrder = 0
      Text = '          '
    end
    object edCodigoDeBarra: TMaskEdit
      Left = 16
      Top = 122
      Width = 148
      Height = 21
      Hint = 
        'O C'#243'digo de Barra, uma vez informado, deve possuir 10 caracteres' +
        '.'
      EditMask = '!9999999999;1; '
      MaxLength = 10
      TabOrder = 2
      Text = '          '
    end
    object edProduto: TEdit
      Left = 16
      Top = 72
      Width = 337
      Height = 21
      Hint = 'Produto '#233' um campo obrigat'#243'rio'
      MaxLength = 60
      TabOrder = 1
    end
  end
end
