unit uCadastroProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, udmCadastroProduto,
  uClasseProdutos, DB;

type
  TAcao = (insert, edit);

  TfrmCadastroProduto = class(TForm)
    pnDados: TPanel;
    pnNavegacao: TPanel;
    btExcluir: TBitBtn;
    btIncluir: TBitBtn;
    btAlterar: TBitBtn;
    btSair: TBitBtn;
    grProdutos: TDBGrid;
    dsCadastroProdutos: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
  private
    listaDeProdutos: TListaProduto;
    procedure mantemDados(acao: TAcao);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroProduto: TfrmCadastroProduto;

implementation

uses
  uManutencaoProduto;

{$R *.dfm}

procedure TfrmCadastroProduto.FormCreate(Sender: TObject);
begin
  listaDeProdutos := TListaProduto.Create;
end;

procedure TfrmCadastroProduto.FormDestroy(Sender: TObject);
begin
  listaDeProdutos.Free;
end;

procedure TfrmCadastroProduto.FormShow(Sender: TObject);
begin
  dsCadastroProdutos.DataSet := listaDeProdutos.carregaDataset;
end;

procedure TfrmCadastroProduto.btSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmCadastroProduto.btExcluirClick(Sender: TObject);
begin
  if(not grProdutos.DataSource.DataSet.IsEmpty) and
    (MessageDlg('Voc� tem certeza que deseja excluir este Produto?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    if listaDeProdutos.excluiProduto(grProdutos.DataSource.DataSet.RecNo -1) then
    else
      MessageBox(0, 'N�o foi poss�vel excluir este produto!', 'Erro na exclus�o dos dados', MB_ICONHAND or MB_OK);
  end;
end;

procedure TfrmCadastroProduto.mantemDados(acao: TAcao);
var fManutencao: TfrmManutencao;
begin
  fManutencao := TfrmManutencao.Create(self);
  try
    fManutencao.ListaDeProduto_Instanciada := listaDeProdutos;

    if acao = insert then
    begin
      fManutencao.pProduto := '';
      fManutencao.pIDCliente := -1;
      fManutencao.pCodBarra := '';
      fManutencao.registro := -1;
    end
    else
      if acao = edit then
      begin
        fManutencao.pProduto := grProdutos.DataSource.DataSet.FieldByName('PRODUTO').AsString;
        fManutencao.pIDCliente := grProdutos.DataSource.DataSet.FieldByName('IDENTIFICADOR_DO_CLIENTE').AsInteger;
        fManutencao.pCodBarra := grProdutos.DataSource.DataSet.FieldByName('CODIGO_DE_BARRA').AsString;
        fManutencao.registro := grProdutos.DataSource.DataSet.RecNo - 1;
      end;

    if fManutencao.ShowModal = mrOk then
    begin
      if acao = edit then
      begin
        if not listaDeProdutos.alteraProduto(TProduto.create(fManutencao.pProduto, fManutencao.pIDCliente)
                                                .comCodigoDeBarra(Trim(fManutencao.pCodBarra)),
                                             grProdutos.DataSource.DataSet.FieldByName('IDENTIFICADOR_DO_CLIENTE').AsInteger) then
          MessageBox(0, 'N�o foi poss�vel efetivar a altera��o do produto', 'Erro na altera��o dos dados', MB_ICONHAND or MB_OK);
      end
      else if acao = insert then
        if not listaDeProdutos.insereProduto(TProduto.create(fManutencao.pProduto, fManutencao.pIDCliente)
                                                .comCodigoDeBarra(Trim(fManutencao.pCodBarra))) then
          MessageBox(0, 'N�o foi poss�vel efetivar a inclus�o do produto', 'Erro na inclus�o dos dados', MB_ICONHAND or MB_OK);
      listaDeProdutos.carregaDataset;
    end;
  finally
    fManutencao.Free;
  end;
end;

procedure TfrmCadastroProduto.btIncluirClick(Sender: TObject);
begin
  mantemDados(insert);
end;

procedure TfrmCadastroProduto.btAlterarClick(Sender: TObject);
begin
  if(not grProdutos.DataSource.DataSet.IsEmpty) then
    mantemDados(edit);
end;

end.
