unit uManutencaoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, uClasseProdutos;

type
  TfrmManutencao = class(TForm)
    pnControles: TPanel;
    pnDados: TPanel;
    edIdentificadorCliente: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edCodigoDeBarra: TMaskEdit;
    edProduto: TEdit;
    btCancelar: TButton;
    btOk: TButton;
    procedure btCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btOkClick(Sender: TObject);
  private
    IDCliente, Posicao_Registro: integer;
    Produto, CodBarra: string;
    ListaDeProdutos: TListaProduto;
    procedure CarregaValores;
    function validaControles: TWinControl;
    procedure SetaValores;
    { Private declarations }
  public
    property pProduto: String read Produto write Produto;
    property pIDCliente: integer read IDCliente write IDCliente;
    property pCodBarra: String read CodBarra write CodBarra;
    property ListaDeProduto_Instanciada: TListaProduto write ListaDeProdutos;
    property registro: integer write Posicao_Registro;
    { Public declarations }
  end;

var
  frmManutencao: TfrmManutencao;

implementation

{$R *.dfm}

procedure TfrmManutencao.btCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmManutencao.SetaValores;
begin
  Produto := edProduto.Text;
  IDCliente := StrToInt(Trim(edIdentificadorCliente.Text));
  CodBarra := Trim(edCodigoDeBarra.Text);
end;

procedure TfrmManutencao.CarregaValores;
begin
  if Trim(Produto) <> '' then
    edProduto.Text := Produto;

  if IDCliente > -1 then
    edIdentificadorCliente.Text := IntToStr(IDCliente);

  if Trim(CodBarra) <> ''  then
    edCodigoDeBarra.Text := Trim(CodBarra);
end;

procedure TfrmManutencao.FormShow(Sender: TObject);
begin
  CarregaValores;
  edIdentificadorCliente.SetFocus;
end;

procedure TfrmManutencao.btOkClick(Sender: TObject);
var controle_com_Erro: TWinControl;
    mensagemErro: String;
begin
  controle_com_Erro := validaControles;
  if controle_com_Erro <> nil then
  begin
    MessageDlg(controle_com_Erro.Hint, mtWarning, [mbOK], 0);
    controle_com_Erro.SetFocus;
    exit;
  end;
  SetaValores;

  mensagemErro := ListaDeProdutos.ValidaOperacao(CodBarra, IDCliente, Posicao_Registro);
  if Trim(mensagemErro) <> '' then
  begin
    MessageDlg(mensagemErro, mtWarning, [mbOK], 0);
    edIdentificadorCliente.SetFocus;
  end
  else
    ModalResult := mrOk;
end;

function TfrmManutencao.validaControles: TWinControl;
begin
  result := nil;

  if Trim(edIdentificadorCliente.Text) = '' then
  begin
    result := edIdentificadorCliente;
    exit;
  end;

  if Trim(edProduto.Text) = '' then
  begin
    result := edIdentificadorCliente;
    exit;
  end;

  if (Trim(edCodigoDeBarra.Text) <> '') and (Length(Trim(edCodigoDeBarra.Text))< 10) then
  begin
    result := edCodigoDeBarra;
    exit;
  end;
end;

end.
